/*
  Multiple Serial test

  Receives from the main serial port, sends to the others.
  Receives from serial port 1, sends to the main serial (Serial 0).

  This example works only with boards with more than one serial like Arduino Mega, Due, Zero etc.

  The circuit:
  - any serial device attached to Serial port 1
  - Serial Monitor open on Serial port 0

  created 30 Dec 2008
  modified 20 May 2012
  by Tom Igoe & Jed Roach
  modified 27 Nov 2015
  by Arturo Guadalupi

  This example code is in the public domain.
*/
#include <SoftwareSerial.h>

int count;
bool teste;
SoftwareSerial mySerial(13, 15);
void setup() {
  // initialize both serial ports:
  
  Serial.begin(9600);

  mySerial.begin(9600);

}

void loop() {
  // read from port 1, send to port 0:
  if (mySerial.available()) {
    int inByte = mySerial.read();
    Serial.write(inByte);
  }

  // read from port 0, send to port 1:
  if (Serial.available()) {
    int inByte = Serial.read();
    mySerial.write(inByte);
  }



  // read from port 1, send to port 0:
 // if (Serial.available()) {
 //   int inByte = Serial.read();
 //   Serial.write(inByte);
 // }
/*
  if(count == 0)
  {
    count = 2000;
    if(teste) 
    {
      teste = 0;
      Serial.write("SERIAL 0");
    }
    else 
    {
      teste = 1;
      mySerial.write("SERIAL 2");
    }
    
  }
  else 
  {
    count--;
    delay(1);
  }*/
}
